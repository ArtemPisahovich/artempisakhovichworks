import csv
import re


def add_year(row):
	year = re.findall((r'\b(\w+)\b'),row['title'])[-1]
	try:
		row['year'] = int(year)
	except:
		row['year'] = 0


def movies_id(movies):
	select_movie_ID = []
	for i in movies:
		select_movie_ID.append(i[0])

	return select_movie_ID


def all_genres(movies):
	genres = []
	for i in movies:
		for j in i[2].split('|'):
			if j not in genres:
				genres.append(j)

	return genres
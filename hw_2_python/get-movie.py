import csv
import re
import help_functions
import argparse
from operator import itemgetter
import time

start_time = time.time()


def add_args():
	""" 
	Function for add arguments to util
	Used argparse

	"""	

	parser = argparse.ArgumentParser(	
									description = "Utils for searching movies by specified criteria",
									epilog = "Example: get-movie.py -N 15 -genre 'Comedy|Horror|Drama|Fantasy' -year_from 2016")

	parser.add_argument('-N', nargs=1, type=int, help='A number of top rated films for each genre')
	parser.add_argument('-genre','--g' ,nargs=1, help='Filter by genre, set by user. It can be multiple. Example: Comedy|Adventure')
	parser.add_argument('-year_from','--yf', nargs=1, type=int, help='The year from which the search can begin')
	parser.add_argument('-year_to','--yt' ,nargs=1, type=int, help='The year before which the search can be carried out')
	parser.add_argument('-regexp','--r', nargs=1, help='Filter (regular expression) on the name of the film')

	args = parser.parse_args()

	return args


def pars_args(args):
	"""
	Function for parsing arguments. 
	A list with specified conditions is drawn up. 
	If the condition is not set, there is a default value.
	"""

	conditions = [100000,[0],0,2100,r'\w']

	if args.N:
		conditions[0] = args.N[0]

	if args.g:
		conditions[1] = args.g[0].split('|')

	if args.yf:
		conditions[2] = args.yf[0]

	if args.yt:
		conditions[3] = args.yt[0]

	if args.r:
		conditions[4] = args.r[0]

	return conditions


def movies_select(conditions):
	""" 
	Function for selecting movies by condition. 
	It also uses an additional add_years() function, which provides a year-end creation. 
	Gives away a list with the righ movies
	"""

	movies = []
	with open('data/movies.csv', newline = '') as movies_file:
		reader = csv.DictReader(movies_file,delimiter=',')
		for row in reader:
			for genre in conditions[1]:

					if re.findall(conditions[4],row['title']) != []:

						if genre == 0:

							help_functions.add_year(row)
									
							if conditions[2] <= row['year'] <= conditions[3] :

								for genre in row['genres'].split('|'):
									
									movies.append([(row['movieId']),(row['title']),genre,(row['year'])])

						else:

							if genre in row['genres'].split('|')  :

								help_functions.add_year(row)
									
								if conditions[2] <= row['year'] <= conditions[3] :
										
									movies += [[(row['movieId']),(row['title']),(genre),(row['year'])]]	
							
	return movies


def get_ratings():
	
	""" 
	Function get list with ratings from data/ratings.csv
	Used csv reader
	"""

	ratings = []
	with open('data/ratings.csv', newline = '') as ratings_file :
		reader = csv.DictReader(ratings_file,delimiter=',')
		for row in reader:
			
			ratings += [[(row['movieId']),(row['rating'])]]

	return ratings


def group_ratings_by_movieID(ratings):

	""" 
	Function grouping ratings by movieID
	Geting ratings_dict where for
	each key there is a list with teo elements:
	the number of ratings and the sum of ratings
	"""
	
	ratings = get_ratings()
	ratings_dict = {}
	for movie in ratings:
		
		movieID = movie[0]
		rating = movie[1]
		
		if movieID in ratings_dict:
			
			ratings_dict[movieID][0] += float(rating)
			ratings_dict[movieID][1] += 1
		else:
			ratings_dict[movieID] = [0,0]
			ratings_dict[movieID][0] = float(rating)
			ratings_dict[movieID][1] = 1

	return ratings_dict


def mean_ratings(movies):
	"""
	Function calculates the mean rating for each movieID.
	Divide the amount by the quantity and
	add to new dictionary
	"""

	ratings_dict = group_ratings_by_movieID(movies)
	mean_ratings = {}

	for key in ratings_dict:
		sum_rating = ratings_dict[key][0]
		number_rating = ratings_dict[key][1] 
		try:
			mean_ratings[key] = sum_rating / number_rating
		except:
			mean_ratings[key] = 0

	return mean_ratings


def add_ratings_to_movies(movies):
	"""
	Function add ratings to movies
	Compare mobieIDs and if they are
	equl, then add ratings at the end movie list
	"""
	
	mean_rat = mean_ratings(movies)

	for movie in movies:
		movieID = movie[0]
		try:
			movie.append(mean_rat[movieID])
		except:
			movie.append(0)
			
	return movies


def sort_movies(movies):
	"""
	Function sorted movies
	Double sorting is carried out to
	display new movies first if tje rating is the same
	"""

	movies = add_ratings_to_movies(movies)

	final_movies = sorted(movies,key=lambda x: x[-2], reverse=True)
	final_movies = sorted(final_movies,key=itemgetter(-1), reverse=True)

	
	return final_movies


def print_movies(movies,conditions):
	"""
	Function output films in accordance with
	the condition for quantity. 
	If genres have not beaen specified, use the helps.all_genres function
	"""
	
	print('genre;title;year;rating')
	
	movies = sort_movies(movies)
	
	top = conditions[0]

	if conditions[1] == 0:
		genres = conditions[1]
	else:
		genres = help_functions.all_genres(movies)

	for genre in genres:
		n = 0
		for _,title,gen,year,rating in movies:

			title = title.rpartition(' ')[0]
			if gen == genre:
				print(f"{gen};{title};{year};{round(rating,1)};")
				n += 1
				if n == top:
					break


if __name__ == '__main__':

	conditions = pars_args(add_args())
	print_movies(movies_select(conditions),conditions)
	

	print('\nExecution time - ', round(time.time() - start_time,3), 'sec.')	



import pandas as pd
import pyarrow.parquet
import argparse



def convert_csv2parquet(csvfile,parquetfile):
	"""Convert csv file to parquet file"""

	try:
		csv = pd.read_csv(csvfile, low_memory=False)
		csv.to_parquet(parquetfile , engine = 'pyarrow',index=False)
		print('Finished')
	except:
		print('Invalid file')

def convert_parquet2csv(parquetfile,csvfile,delimiter = ','):
	"""Convert parquet file to csv file"""

	try:
		parquet = pd.read_parquet(parquetfile, engine = 'pyarrow')
		parquet.to_csv(csvfile, sep = delimiter, index=False )
		print('Finished')
	except:
		print('Invalid file')

def print_parquet_schema(parquetfile):
	"""Print parquet file schema"""

	try:
		table = pyarrow.parquet.read_table(parquetfile)
		print(table)
		return table
	except:
		print('Invalid file')


def pars():
	"""Pars arguments"""

	parser = argparse.ArgumentParser(	
									description = ' Converter from csv to parquet AND parquet to csv',
									epilog = "Example: convert.py --csv2parquet D:/myfolder/file.csv D:/myfolder/file.parquet")

	parser.add_argument('--csv2parquet', nargs=2, help = 'convert CSV file to PARQUET file')
	parser.add_argument('--parquet2csv', nargs=2, help = 'convert PARQUET file to CSV file')
	parser.add_argument('--get_schema', nargs=1, help = 'print schema parquet file')
	
	args = parser.parse_args()

	return args

def main(args):
	"""Execution of the given arguments"""

	if args.csv2parquet:
		try:
			convert_csv2parquet(args.csv2parquet[0],args.csv2parquet[1])
		except:
			print('The arguments must contain the path to the file')
			
	if args.parquet2csv:
		try:
			convert_parquet2csv(args.parquet2csv[0],args.parquet2csv[1])
		except:
			print('The arguments must contain the path to the file')
		

	if args.get_schema:
		try:
			print_parquet_schema(args.get_schema[0])
		except:
			print('The argument must contain the path to the file')
	
	print('Use --help or -h to output documentation ')



if __name__ == '__main__':

	main(pars())
	



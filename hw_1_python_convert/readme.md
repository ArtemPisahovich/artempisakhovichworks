# Converter.py

## Usage util

`convert.py [-h] [--help] [--csv2parquet] [--parquet2csv] [--get_schema]`


## Optional 

### Show help message

`convert.py [--help] [-h]`

### Convert csv to parquet

`convert.py --csv2parquet <src-filename> <dst-filename>`

### Convert parquet to csv

`convert.py --parquet2csv <src-filename> <dst-filename>`

### Print parquet file schema

`convert.py --get_schema <filename>`

## Examples

### Convert csv to parquet

`convert.py --csv2parquet myfolder/test.csv myfolder/test.parquet`

### Print parquet file schema

`convert.py --get_schema myfolder/test.parquet`



from urllib.request import Request, urlopen
from urllib.error import URLError, HTTPError
import csv
import pandas as pd
import time


class Analyzer:

	def __init__(self,site,file):
		self.site = site
		self.file = file

		with open(self.file, 'r',newline = '') as file:
            opened_file = file.readlines()
        if opened_file == []:
            with open(self.file, 'a',newline = '') as file:
                writer = csv.writer(file)
                writer.writerows([["id","error","start_time","end_time"]])
	
	def chek(self):
	
		req = Request(self.site)

		try:
			response = urlopen(req)
		except HTTPError as e:
			error = e.code
		except URLError as e:
			error = e.reason 
		else:
			error = 'ok'

		return error
		

	def filing(self):
		self.error = self.chek()

		if str(self.error) == '[Errno 11004] getaddrinfo failed':
			self.error = "Loss of internet connection"

		def take_last_record(file):
			with open(file,'r') as f:
				opened_file = f.readlines()
				last_record = opened_file[-1].split(',')
				try:
					last_record[0] = int(last_record[0])
				except:
					last_record[0] = 0
			return last_record

		last_record = take_last_record(self.file)
		

		if self.error == 'ok':
			if last_record[-2] != '' and last_record[-1] == " \n":
				df = pd.read_csv(self.file)
				df.at[int(last_record[0])-1,"end_time"] = time.ctime()
				df.to_csv(self.file, index = False)
				

		if self.error != 'ok':
			if last_record[-2] != '' and last_record[-1] != " \n":
				with open(self.file, 'a',encoding='utf8',newline = '') as file:
					writer = csv.writer(file)
					writer.writerows([[int(last_record[0])+1, str(self.error),time.ctime()," "]])
		
		print(self.error)

if __name__ == '__main__':

	site = "https://onliner.by"
	file = "C:/Users/User/Desktop/Maxbitsolution/error_base.csv"
	
	onliner = Analyzer(site,file)

	while True:
		onliner.filing()
		time.sleep(60)
		
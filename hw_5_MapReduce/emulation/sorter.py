import sys
from operator import itemgetter





def sorter(movies):
	norm_movies = []
	try:
		for genre,title_year in movies:
			title = title_year.split('|')[0]
			year = title_year.split('|')[1]
			year = year.rstrip()
			norm_movies.append([genre,title,year])
	except:
		1

	final_movies = sorted(norm_movies,key=lambda x: x[-2], reverse=False)
	final_movies = sorted(final_movies,key=itemgetter(-1), reverse=True)

	return final_movies

def main():
	movies = []
	for movie in sys.stdin:
		movies.append(movie.split('\t'))

	final_movies = sorter(movies)

	for genre,title,year in final_movies:
		print(f"{genre};{title};{year}")



if __name__ == "__main__":

	main()
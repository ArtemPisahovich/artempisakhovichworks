import sys 
import re
import argparse

def add_args():
	""" 
	Function for add arguments to util
	Used argparse

	"""	

	parser = argparse.ArgumentParser()

	
	parser.add_argument('-genre','--g' )
	parser.add_argument('-year_from','--yf')
	parser.add_argument('-year_to','--yt')
	parser.add_argument('-regexp','--r')

	args = parser.parse_args()

	return args


def pars_args(args):
	
	conditions = [[0],1000,2100,'.+']
	if args.g:
		if args.g != 'no':
			conditions[0] = args.g.split('|')
		
	if args.yf:
		if args.yf != 'no':
			conditions[1] = int(args.yf)

	if args.yt:
		if args.yt != 'no':
			conditions[2] = int(args.yt)

	if args.r:
		if args.r != 'no':
			conditions[3] = args.r

	return conditions



def map(key,value):

	
	title = value.rpartition(' ')[0]
	
	year  = re.findall((r'\b(\w+)\b'),value)[-1]

	try:
		year = int(year)
	except:
		year = 0
	
	genres = key



	films = []
	for genre in genres.split('|'):

		map_movie = [genre.rstrip(),title,year]
		films.append(map_movie)


	return films


def filter(conditions,genre,title,year):

	if conditions[1] <= year <= conditions[2]:

		if re.findall(conditions[3],title) != []:
						
			if conditions[0] == [0]:
							
				return genre,title,year
							

			else:
				for gen in conditions[0]:
					if genre == gen:

						return genre,title,year
									

def main():
	conditions = pars_args(add_args())

	for movie in sys.stdin:
		try:
			mov = movie.split(',')
			value,key = mov[1:-1][0],mov[-1]

			for genre,title,year in map(key,value):

				genre,title,year = filter(conditions,genre,title,year)

				print(f'{genre}\t{title}|{year}')
				
		except:
			pass

if __name__ == '__main__':

	main()




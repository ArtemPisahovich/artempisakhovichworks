
# MapReduce
## MapReduce-emulation.sh & MapReduce-hadoop.sh
[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)
Movie-picking util
The DataSet was taken from [MovieLens|GroupLens.org](https://grouplens.org/datasets/movielens/)

## Conditions for choosings movies

- Number of films
- Choosing a genre
- Choice of the year of the film's release
- Choise by regular expression

##### You need to work
###### MapReduce-emulation.sh
For the util to work on your computer should be: [Python3](https://www.python.org/)
###### MapReduce-hadoop.sh 
Google cloud clusters


## Usage util

`bash MapReduce-emulation.sh\MapReduce-hadoop.sh [-h] [--help] [-N] [-genre] [-year_for] [-year_to] [-regexp]`


## Optional 

### Show help message

`bash MapReduce-emulation.sh\MapReduce-hadoop.sh [--help] [-h]`

``` 
Utils for searching movies by specified criteria

optional arguments:
  -h, --help            show this help message and exit
  -N , --n              A number of top rated films for each genre
  -genre , --g          Filter by genre, set by user. It can be multiple.
                        Example: Comedy|Adventure
  -year_from, --yf      The year from which the search can begin
  -year_to , --yt       The year before which the search can be carried out
  -regexp , -r          Filter (regular expression) on the name of the film

Example: sql-movie.py -N 15 -genre 'Comedy|Horror|Drama|Fantasy' -year_from
2016
```
### Number of films

A number of top rated films for each genre

`bash MapReduce-emulation.sh\MapReduce-hadoop.sh -N <number>`
```
genre;title;year
Drama;A Quiet Place;2018
Horror;A Quiet Place;2018
Thriller;A Quiet Place;2018
Adventure;A Wrinkle in Time;2018
Children;A Wrinkle in Time;2018
Fantasy;A Wrinkle in Time;2018
Sci-Fi;A Wrinkle in Time;2018
Mystery;Annihilation;2018
Action;Ant-Man and the Wasp;2018
Comedy;Ant-Man and the Wasp;2018
Crime;BlacKkKlansman;2018
Animation;Bungo Stray Dogs: Dead Apple;2018
Romance;Mamma Mia: Here We Go Again!;2018
Documentary;Spiral;2018
(no genres listed);A Christmas Story Live!;2017
War;Darkest Hour;2017
Western;The Beguiled;2017
IMAX;Star Wars: Episode VII - The Force Awakens;2015
Musical;Strange Magic;2015
Film-Noir;Bullet to the Head;2012
```
### Choosing a genre

Filter by genre, set by user. It can be multiple. Example: "Comedy|Adventure"

`bash MapReduce-emulation.sh\MapReduce-hadoop.sh -genre <selected_genre>`

```
$ bash MapReduce-emulation.sh -genre "Adventure|Comedy"  -N 3
genre;title;year
Adventure;A Wrinkle in Time;2018
Adventure;Alpha;2018
Adventure;Annihilation;2018
Comedy;BlacKkKlansman;2018
Comedy;Blockers;2018
Comedy;Boundaries;2018

```
### Choice of the year of the film's release
The year from which the search can begin or The year before which the search can be carried out


`bash MapReduce-emulation.sh\MapReduce-hadoop.sh -year_from <year> -year_to <year>`

```
bash MapReduce-emulation.sh -N 4 -year_from 1970 -year_to 1980 -genre "Drama|Romance"
genre;title;year
Drama;Altered States;1980
Drama;American Gigolo;1980
Drama;Breaker Morant;1980
Drama;Fame;1980
Romance;Somewhere in Time;1980
Romance;The Idolmaker;1980
Romance;Moscow Does Not Believe in Tears (Moskva slezam ne verit);1979
Romance;Barry Lyndon;1975
```


### Choise by regular expression

Filter (regular expression) on the name of the film

`bash MapReduce-emulation.sh\MapReduce-hadoop.sh -regexp <regular_expression>`

```
$ bash MapReduce-emulation.sh -regexp Moscow
genre;title;year
Comedy;Police Academy: Mission to Moscow;1994
Comedy;Moscow on the Hudson;1984
Comedy;Don Camillo in Moscow;1965
Crime;Police Academy: Mission to Moscow;1994
Drama;Moscow on the Hudson;1984
Drama;Moscow Does Not Believe in Tears (Moskva slezam ne verit);1979
Romance;Moscow Does Not Believe in Tears (Moskva slezam ne verit);1979

```


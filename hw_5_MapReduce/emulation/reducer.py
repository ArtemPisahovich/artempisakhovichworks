import sys
import json

import argparse


def add_args():

	parser = argparse.ArgumentParser()

	parser.add_argument('-N')

	args = parser.parse_args()

	return args

def pars_args(args):

	if args.N:
		if args.N != 'no':
			number = int(args.N)
		else:
			number = 100000

	return number


def reduce(red_dict,number):

	print("genre;title;year")

	for genre in red_dict:

		if len(genre) > 30:
			break

		n = 0
		
		for movie in red_dict[genre].split("], ["):
			try:
				title,year = movie.split("', '")
				title = title.lstrip("[['")

				nyear = ''
				for i in year:
					try:
						int(i)
						nyear +=i
					except:
						None

				print(f"{genre};{title};{nyear}")
				n +=1
				if n >= number :
					break
			except:
				None
		
def main(number):
	reduce_dict = {}
	for line in sys.stdin:
		key,value = line.split("\t")
		reduce_dict[key] = value

	reduce(reduce_dict,number)
		

if __name__ == '__main__':

	number = pars_args(add_args())
	main(number)
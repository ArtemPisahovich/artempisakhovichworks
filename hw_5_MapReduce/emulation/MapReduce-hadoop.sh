hdfs dfs -mkdir /movies/

hdfs dfs -put movies.csv /movies/

n="no"
genre="no"
year_from="no"
year_to="no"
regexp="no"


while [ -n "$1" ]
do
	case "$1" in
		-N) n=$2
			shift;;
		-genre) genre=$2
				shift;;
		-year_from) year_from=$2
					shift;;
		-year_to)   year_to=$2
					shift;;
		-regexp)   regexp=$2
					shift;;
		-help) python sql-movie.py --help
				exit
				
		
esac
shift
done

hdfs dfs -rm -R output

yarn jar /usr/lib
/hadoop/hadoop-streaming.jar -input /movies/movies.csv -output /movies/output -file mapper.py reducer.py \
-mapper "python3 mapper.py -genre $genre -year_from $year_from -year_to $year_to -regexp $regexp" \
 -reducer "python3 reducer.py -N $n" 

 





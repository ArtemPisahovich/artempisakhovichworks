import csv


def all_genres():

	"""
	Functions is used when user has not
	specified desired genres 
	"""
	uniq_genre = []
	with open('admin/database/all_genres.txt') as file:
		for line in file:
			uniq_genre.append(line.strip('\n'))

	return uniq_genre


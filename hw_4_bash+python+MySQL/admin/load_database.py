"""
The script deploys the database using set of functions
from dictionary 'database'
If database alredy deployed the script does not run
"""

from database.insert_into_movies import insert_into_movies
from database.insert_into_ratings import insert_into_ratings 
from database.create_database import create_database
from database.create_tables import create_tables
from database.insert_into_movies_with_rating import insert_into_movies_with_rating
from database.usp_find_top_rated import usp_find_top_rated
from database.insert_into_std_movies import insert_into_std_movies
import user


def load_database(user):
	
	create_database(user)
	
	create_tables(user)
			
	insert_into_std_movies(user)
	
	insert_into_ratings(user)

	insert_into_movies(user)
		
	insert_into_movies_with_rating(user)
		
	usp_find_top_rated(user)
		
load_database(user)

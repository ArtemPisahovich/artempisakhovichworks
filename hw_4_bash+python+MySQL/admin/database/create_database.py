from mysql.connector import connect, Error


def create_database(user):
	"""
	Function creates a database
	Connection with mysql.connector
	"""
	try:
		with connect(
			host = user.host,
					user=user.user,
					password=user.password,) as connection:

			create_db_query = f"""CREATE DATABASE {user.database}"""
			with connection.cursor() as cursor:
				cursor.execute(create_db_query)
				
	except Error:
		pass
		
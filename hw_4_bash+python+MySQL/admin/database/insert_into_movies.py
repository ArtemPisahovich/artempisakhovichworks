import csv
import re
from mysql.connector import connect, Error


def insert_into_movies(user):
	"""
	Functions adds data to the movie table.
	"""
	
	try:
		with connect(host = user.host,
					user=user.user,
					password=user.password,
					database=user.database) as connection:
			insert_movies = """
INSERT IGNORE INTO movies(movie_id,genre,title,year)
with recursive cte1 as(
select 
		movie_id,
		genres,
		replace(title,substring_index(title, ' ',-1),'') as title,
		replace(replace(substring_index(title, ' ',-1),'(',''),')','') as year 
FROM std_movies
),
cte2 as (
Select 
		movie_id,
		title,
		year,
     CASE
		WHEN LOCATE('|',genres ) <> 0 THEN SUBSTRING(genres,1,LOCATE('|',genres)-1)
         ELSE genres
	 END AS Word,
     CASE
		WHEN LOCATE('|',genres) <> 0 THEN SUBSTRING(genres,LOCATE('|',genres)+1, length(genres) - LOCATE('|', genres))
		ELSE ''
	END AS genres
 FROM cte1
 UNION ALL
 Select 
	movie_id,
	title,
    year,
     CASE
		WHEN LOCATE('|',genres ) <> 0 THEN SUBSTRING(genres,1,LOCATE('|',genres)-1)
         ELSE genres
	 END AS Word,
     CASE
		WHEN LOCATE('|',genres) <> 0 THEN SUBSTRING(genres,LOCATE('|',genres)+1, length(genres) - LOCATE('|', genres))
		ELSE ''
	END AS genres
FROM cte2
WHERE genres <> '')

Select
		movie_id,
        Word as genre,
        title,
		year
From cte2
			"""
			with connection.cursor() as cursor:
				cursor.execute(insert_movies)
				connection.commit()
	except Error as e:
		print(e)
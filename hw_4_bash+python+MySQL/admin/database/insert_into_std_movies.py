import csv
import re
from mysql.connector import connect, Error


def insert_into_std_movies(user):
	"""
	Functions adds data to the movie table.
	"""
	
	movies = []
	uniq_genre = []
	with open('database/data/movies.csv', newline='') as movies_file:
		reader = csv.DictReader(movies_file,delimiter=',')
	try:
		for row in reader:
			movies +=[((row['movieId']),(row['title']),(row['genres']))]
			for genre in (row['genres']).split('|'):
				if genre not in uniq_genre:
					uniq_genre.append(genre)
	except:
		print('error')
	

	with open('database/all_genres.txt','w') as file:
		for genre in uniq_genre:
			print(f"{genre}", file=file)
				
	
	try:
		with connect(host = user.host,
					user=user.user,
					password=user.password,
					database=user.database) as connection:
			insert_movies = """
			INSERT INTO std_movies(movie_id,title,genres)
			VALUES	(%s,%s,%s)
			"""
			with connection.cursor() as cursor:

				cursor.executemany(insert_movies,movies)
				connection.commit()
	except Error as e:
		pass


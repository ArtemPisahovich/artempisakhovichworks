from mysql.connector import connect, Error

def create_tables(user):

	"""
	Function creates tables: 'movies','ratings','movies_with_rating'
	"""
	try:
		with connect(
			host = user.host,
			user=user.user,
			password=user.password,
			database=user.database) as connection:
			create_movies_table = """
			
			CREATE TABLE movies(
			id INT AUTO_INCREMENT PRIMARY KEY,
			movie_id INT,
			genre VARCHAR(50),
			title VARCHAR(200),
			year INT)
			"""
			with connection.cursor() as cursor:
				cursor.execute(create_movies_table)
				connection.commit()
	except Error as e:
		pass

	try:
		with connect(
			host = user.host,
			user=user.user,
			password=user.password,
			database=user.database) as connection:

			create_ratings_table = """

			CREATE TABLE std_movies(
			id INT AUTO_INCREMENT PRIMARY KEY,
			movie_id INT,
			title VARCHAR(200),
			genres VARCHAR(100)
			)
			"""
			with connection.cursor() as cursor:
				cursor.execute(create_ratings_table)
				connection.commit()
	except Error as e:
		print(e)

	

	try:
		with connect(
			host = user.host,
			user=user.user,
			password=user.password,
			database=user.database) as connection:
			create_ratings_table = """

			CREATE TABLE movies_with_rating(
			id INT AUTO_INCREMENT PRIMARY KEY,
			genre VARCHAR(50),
			title VARCHAR(200),
			year INT,
			rating FLOAT
			)
			"""
			with connection.cursor() as cursor:
				cursor.execute(create_ratings_table)
				connection.commit()
	except Error as e:
		pass


	try:
		with connect(
			host = user.host,
			user=user.user,
			password=user.password,
			database=user.database) as connection:
			create_ratings_table = """

			CREATE TABLE ratings(
			id INT AUTO_INCREMENT PRIMARY KEY,
			movie_id INT,
			rating FLOAT
			)
			"""
			with connection.cursor() as cursor:
				cursor.execute(create_ratings_table)
				connection.commit()
	except Error:
		flag = 1
		return flag
		pass


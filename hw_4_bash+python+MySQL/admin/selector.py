from mysql.connector import connect, Error


def call_to_db(n,genre,year_from,year_to,regexp,user):

	"""
	Fucntion connect to database and use
	procedure 'Selector' with specified arguments
	"""
	print('genre;title;year;rating;')
	for gen in genre:
		try:
			with connect(
				host = user.host,
				user = user.user,
				password = user.password,
				database = user.database) as connection:
				select_movies = f"""
				CALL Selector({n},'{gen}',{year_from},{year_to},'{regexp}')
				"""
				with connection.cursor() as cursor:
					cursor.execute(select_movies)
					for genre,title,year,rating in cursor.fetchall():
						print(f"{genre};{title};{year};{rating};")
		except Error as e:
			print('Call')
			print(e)

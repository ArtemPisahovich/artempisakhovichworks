from mysql.connector import connect, Error
import user

def drop_database(user):
	try:
		with connect(
			host = user.host,
			user = user.user,
			password = user.password,
			database = user.database) as connection:
			alter = f"""
			DROP DATABASE {user.database}
			"""
			with connection.cursor() as cursor:
				cursor.execute(alter)
				connection.commit()

	except Error as e:
		print(e)

drop_database(user)
#!/bin/bash
n="no"
genre="no"
year_from="no"
year_to="no"
regexp="no"
setupdb="no"
while [ -n "$1" ]
do
	case "$1" in
		-N) n=$2
			shift;;
		-genre) genre=$2
				shift;;
		-year_from) year_from=$2
					shift;;
		-year_to)   year_to=$2
					shift;;
		-regexp)   regexp=$2
					shift;;
		-setupdb)  setupdb=1;;
		-help) python sql-movie.py --help
				exit
				
		
esac
shift
done
if [[ $setupdb = 1 ]]; then
	cp user.py admin/user.py
	cd admin/database
	curl -O 'https://files.grouplens.org/datasets/movielens/ml-latestsmall.zip' >&2
	unzip ml-latest-small.zip
	rm -v ml-latest-small.zip
	mv ml-latest-small data 
	cd data
	rm -v tags.csv
	rm -v README.txt
	rm -v links.csv
	cd ..
	cd ..
	python drop_database.py
	echo "Loading"
	python load_database.py
	cd ..
	cd admin/database/data
	rm -v movies.csv
	rm -v ratings.csv
	cd ..
	rmdir data
	cd ..
	cd ..
	
	
fi
python sql-movie.py -N $n -genre $genre -year_from $year_from -year_to $year_to -regexp $regexp



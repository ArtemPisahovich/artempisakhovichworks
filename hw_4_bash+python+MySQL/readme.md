
# get-movie.sh
[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)
Movie-picking util
The DataSet was taken from [MovieLens|GroupLens.org](https://grouplens.org/datasets/movielens/)

## Conditions for choosings movies

- Number of films
- Choosing a genre
- Choice of the year of the film's release
- Choise by regular expression

##### You need to work
For the util to work on your computer should be: [Python3](https://www.python.org/) , [MySQL](https://dev.mysql.com/downloads/mysql/)



## Usage util

`python sql-movie.py [-h] [--help] [-N] [-genre] [-year_for] [-year_to] [-regexp]`

### User.py
You need to insert data for your database
Example:
```
host='localhost'
user='root'
password='jdaNA35'
database='movies'
```

## Optional 

### Show help message

`python sql-movie.py [--help] [-h]`

``` 
Utils for searching movies by specified criteria

optional arguments:
  -h, --help            show this help message and exit
  -N , --n              A number of top rated films for each genre
  -genre , --g          Filter by genre, set by user. It can be multiple.
                        Example: Comedy|Adventure
  -year_from, --yf      The year from which the search can begin
  -year_to , --yt       The year before which the search can be carried out
  -regexp , -r          Filter (regular expression) on the name of the film

Example: sql-movie.py -N 15 -genre 'Comedy|Horror|Drama|Fantasy' -year_from
2016
```
#### Warning
The first execution will be longer than the others. The database will be loaded
### Number of films

A number of top rated films for each genre

`python sql-movie.py -N <number>`
```
$ python sql-movie.py -N 1
genre;title;year;rating
Documentary;Won't You Be My Neighbor?;2018;5.0;
Animation;Loving Vincent;2017;5.0;
Crime;Loving Vincent;2017;5.0;
Drama;Loving Vincent;2017;5.0;
Adventure;Ice Age: The Great Egg-Scapade;2016;5.0;
Children;Ice Age: The Great Egg-Scapade;2016;5.0;
Comedy;Ice Age: The Great Egg-Scapade;2016;5.0;
Sci-Fi;SORI: Voice from the Heart;2016;5.0;
Romance;All Yours;2016;5.0;
Horror;The Girl with All the Gifts;2016;5.0;
Thriller;The Girl with All the Gifts;2016;5.0;
War;Battle For Sevastopol;2015;5.0;
Mystery;The Editor;2015;5.0;
Fantasy;L.A. Slasher;2015;5.0;
Action;Tokyo Tribe;2014;5.0;
Musical;Holy Motors;2012;5.0;
IMAX;Happy Feet Two;2011;5.0;
(no genres listed);Death Note: Desu nôto;2007;5.0;
Western;Trinity and Sartana Are Coming;1972;5.0;
Film-Noir;Rififi (Du rififi chez les hommes);1955;4.8;

```
### Choosing a genre

Filter by genre, set by user. It can be multiple. Example: "Comedy|Adventure"

`python sql-movie.py -genre <selected_genre>`

```
$ python sql-movie.py -N 5 -genre Mystery
genre;title;year;rating
Mystery;The Editor;2015;5.0;
Mystery;PK;2014;5.0;
Mystery;Holy Motors;2012;5.0;
Mystery;Scooby-Doo! Curse of the Lake Monster;2010;5.0;
Mystery;Colourful (Karafuru);2010;5.0;

```
### Choice of the year of the film's release
The year from which the search can begin

`python sql-movie.py -year_from <year>`
```
$ python sql-movie.py -N 2 -genre "Drama|Adventure" -year_from 2002
genre;title;year;rating
Drama;Loving Vincent;2017;5.0;
Drama;SORI: Voice from the Heart;2016;5.0;
Adventure;Ice Age: The Great Egg-Scapade;2016;5.0;
Adventure;Delirium;2014;5.0;

```

The year before which the search can be carried out

`python sql-movie.py -year_to <year>`

```
$ python sql-movie.py -N 2 -genre "Drama|Adventure" -year_to 1966
genre;title;year;rating
Drama;Man and a Woman, A (Un homme et une femme);1966;5.0;
Drama;King of Hearts;1966;5.0;
Adventure;Vovka in the Kingdom of Far Far Away;1965;5.0;
Adventure;Carry on Cabby;1963;4.5;

```

##### These two conditions can be multiple

```
$ python sql-movie.py -N 3 -genre Comedy -year_from 1999 -year_to 2000
genre;title;year;rating
Comedy;Bossa Nova;2000;5.0;
Comedy;I'm the One That I Want;2000;5.0;
Comedy;Nine Lives of Tomas Katz, The;2000;5.0;

```
### Choise by regular expression

Filter (regular expression) on the name of the film

`python sql-movie.py -regexp <regular_expression>`

```
$ python sql-movie.py -N 3 -genre Horror -regexp Dog
genre;title;year;rating
Horror;Dog Soldiers;2002;4.7;
Horror;Devil Dog: The Hound of Hell;1978;4.0;
Horror;Barking Dogs Never Bite (Flandersui gae);2000;3.5;

```

## Examples
#### You can use all conditions together 

`python sql-movie.py -N 3 -genre "Adventure|Drama" -year_from 2010 -year_to 2015 -regexp Love`
```
$ python sql-movie.py -N 3 -genre "Adventure|Drama" -year_from 2010 -year_to 2015 -regexp Love
genre;title;year;rating
Drama;One I Love, The;2014;5.0;
Drama;Only Lovers Left Alive;2013;5.0;
Drama;Love;2015;4.5;
Adventure;God Loves Caviar;2012;2.0;
Adventure;The Lovers;2015;1.5;

```


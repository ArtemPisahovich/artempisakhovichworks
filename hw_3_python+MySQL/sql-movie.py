from admin.help_functions import all_genres
import argparse
from mysql.connector import connect, Error
import user
from admin.selector import call_to_db
from admin.load_database import load_database





def add_args():
	""" 
	Function for add arguments to util
	Used argparse

	"""	

	parser = argparse.ArgumentParser(	
									description = "Utils for searching movies by specified criteria",
									epilog = "Example: get-movie.py -N 15 -genre 'Comedy|Horror|Drama|Fantasy' -year_from 2016")

	parser.add_argument('-N', nargs=1, type=int, help='A number of top rated films for each genre')
	parser.add_argument('-genre','--g' ,nargs=1, help='Filter by genre, set by user. It can be multiple. Example: Comedy|Adventure')
	parser.add_argument('-year_from','--yf', nargs=1, type=int, help='The year from which the search can begin')
	parser.add_argument('-year_to','--yt' ,nargs=1, type=int, help='The year before which the search can be carried out')
	parser.add_argument('-regexp','--r', nargs=1, help='Filter (regular expression) on the name of the film')

	args = parser.parse_args()

	return args


def pars_args(args):
	"""
	Function for parsing arguments. 
	A list with specified conditions is drawn up. 
	If the condition is not set, there is a default value.
	"""

	conditions = [100000,0,0,2100,'.+']

	if args.N:
		conditions[0] = args.N[0]

	if args.g:
		conditions[1] = args.g[0].split('|')
	else:
		conditions[1] = all_genres()
	if args.yf:
		conditions[2] = args.yf[0]

	if args.yt:
		conditions[3] = args.yt[0]

	if args.r:
		conditions[4] = args.r[0]

	return conditions





def print_movies(conditions):
	"""
	Make a query to the database and print what it returns
	Uses function 'call_to_db' from admin.selector 
	"""
	n = conditions[0]
	genre = conditions[1]
	year_from = conditions[2]
	year_to = conditions[3]
	regexp = conditions[4]

	call_to_db(n,genre,year_from,year_to,regexp,user)

if __name__ == "__main__":

	conditions = pars_args(add_args())
	load_database(user)
	print_movies(conditions)
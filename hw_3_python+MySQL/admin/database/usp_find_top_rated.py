from mysql.connector import connect, Error


def usp_find_top_rated(user):
	"""
	Function add to database stored procedure 'Selector'
	A stored procedure that selects the highest-rated movies 
	at the users's requests
	"""
	try:
		with connect(
					host=user.host,
					user=user.user,
					password=user.password,
					database=user.database) as connection:
			create_procedure = """
CREATE PROCEDURE Selector(IN var1 INT, IN var2 VARCHAR(50), IN var3 INT, IN var4 INT, IN var5 VARCHAR(50))
BEGIN
SELECT genre,title,year,ROUND(rating,1)
				FROM movies_with_rating
				WHERE genre = var2 
					  AND year >= var3
					  AND year <= var4
					  AND REGEXP_SUBSTR(title,var5) != 'None'
				ORDER BY rating DESC, year DESC
				LIMIT var1;
			END 
			"""
			with connection.cursor() as cursor:
				cursor.execute(create_procedure)
				connection.commit()
	except Error as e:
		print(e)
		


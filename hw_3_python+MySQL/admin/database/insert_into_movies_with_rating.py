from mysql.connector import connect, Error

def insert_into_movies_with_rating(user):
	"""
	Function connect to database and adds
	the processed data to 'movies_with _rating' table using a SQL query
	"""
	try:
		with connect(
			host = user.host,
			user=user.user,
			password=user.password,
			database=user.database) as connection:
			insert = """
			INSERT INTO movies_with_rating(genre,title,year,rating)
			SELECT genre,title,year,rating
					FROM movies
					INNER JOIN (SELECT movie_id, AVG(rating) as rating
								FROM ratings
								GROUP BY movie_id) as mean_ratings
							 ON movies.movie_id = mean_ratings.movie_id
			"""
			with connection.cursor() as cursor:
				cursor.execute(insert)
				connection.commit()

	except Error as e:
		print(e)


		
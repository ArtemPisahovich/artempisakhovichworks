import csv
import re
from mysql.connector import connect, Error


def insert_into_movies(user):
	"""
	Functions adds data to the movie table.
	Used helper function 'add_year' which addds yaers to separate column
	"""

	def add_year(row):
		year = re.findall((r'\b(\w+)\b'),row['title'])[-1]
		try:
			row['year'] = int(year)
		except:
			row['year'] = None


	movies = []
	uniq_genre = []
	with open('admin/database/data/movies.csv', newline='') as movies_file:
		reader = csv.DictReader(movies_file,delimiter=',')
		for row in reader:
			for genre in row['genres'].split('|'):
				add_year(row)
				movie_id = row['movieId']
				title = row['title'].rpartition(' ')[0]
				year = row['year']
				movies +=[(int(movie_id),genre,title,year)]
				
				if genre not in uniq_genre:
					uniq_genre.append(genre)
				
	
	try:
		with connect(host = user.host,
					user=user.user,
					password=user.password,
					database=user.database) as connection:
			insert_movies = """
			INSERT INTO movies( movie_id, genre, title, year)
			VALUES	(%s,%s,%s,%s)
			"""
			with connection.cursor() as cursor:
				cursor.executemany(insert_movies,movies)
				connection.commit()
	except Error as e:
		pass

	
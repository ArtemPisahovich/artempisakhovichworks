import csv
from mysql.connector import connect, Error


def insert_into_ratings(user):
	"""
	Function connects to database and adds value
	to 'ratings' table
	"""
	
	ratings = []
	with open('admin/database/data/ratings.csv', newline = '') as ratings_file :
		reader = csv.DictReader(ratings_file,delimiter=',')
		for row in reader:
			ratings += [(int((row['movieId'])),float((row['rating'])))]


	try:
		with connect(
					host = user.host,
					user=user.user,
					password=user.password,
					database=user.database) as connection:
			add_ratings = """
			INSERT INTO ratings(movie_id, rating)
			VALUES (%s,%s)
			"""
			with connection.cursor() as cursor:
				cursor.executemany(add_ratings,ratings)
				connection.commit()

	except Error as e:
		pass
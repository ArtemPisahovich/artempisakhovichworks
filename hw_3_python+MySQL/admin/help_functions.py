import csv


def all_genres():

	"""
	Functions is used when user has not
	specified desired genres 
	"""
	genres = []
	with open('admin/database/data/movies.csv', newline='') as movies_file:
		reader = csv.DictReader(movies_file,delimiter=',')
		for row in reader:
			for genre in row['genres'].split('|'):
				genres += [genre]


	uniq_genre = []
	for i in genres:
		for j in i.split('|'):
			if j not in uniq_genre:
				uniq_genre.append(j)
	
	return uniq_genre
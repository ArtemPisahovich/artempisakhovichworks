"""
The script deploys the database using set of functions
from dictionary 'database'
If database alredy deployed the script does not run
"""

from admin.database.insert_into_movies import insert_into_movies
from admin.database.insert_into_ratings import insert_into_ratings 
from admin.database.create_database import create_database
from admin.database.create_tables import create_tables
from admin.database.insert_into_movies_with_rating import insert_into_movies_with_rating
from admin.database.usp_find_top_rated import usp_find_top_rated


def load_database(user):
	create_database(user)
		
	if create_tables(user) == 1:
		pass
	else:
		insert_into_movies(user)
		insert_into_ratings(user)
		insert_into_movies_with_rating(user)
		usp_find_top_rated(user)
import argparse
import functions

def add_args():
	""" 
	Function for add arguments to util
	Used argparse

	"""	

	parser = argparse.ArgumentParser(	
									description = "Utility for storing, adding and analyzing orders")

	parser.add_argument('-add', help='Add order to base')
	parser.add_argument('-analysis','--a',help='Data output for every hour for the last 3 days')
	
	
	args = parser.parse_args()

	return args

def pars_args(args):
	
	"""
	Function for performing specified actions
	"""

	if args.add:
		functions.add_order(args.add)
		print('Changes added!')

	if args.a:
		functions.output_analysis()

if __name__ == "__main__":

	pars_args(add_args())





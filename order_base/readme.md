
# Order Base
[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)
Utility for storing, adding and analyzing orders

##### You need to work
For the util to work on your computer should be: [Python3](https://www.python.org/)
### BASE.CSV
```
order_id,date,buyer_name,product,price
1,2021_11_7_11:22,Ivanov_Ivan,Iphone_XR,1000
2,2021_11_9_09:22,Petrov_Petr,Iphone_7,200
3,2021_11_10_10:35,Lopatov_Andrey,Iphone_8,500
4,2021_11_10_11:32,Kolonov_Petr,Iphone_XR,1000
5,2021_11_10_12:34,Novikov_Nikita,Samsung_S9,800
6,2021_11_11_15:35,Kac_Alexander,Iphone_12,1500
7,2021_11_11_15:44,Kac_Alexander,Case_Iphone_12,20
8,2021_11_11_15:56,Pleytuh_Vasiliy,Iphone_8,600
9,2021_11_11_16:48,Stepancov_Danil,OnePlus_8,500
10,2021_11_11_16:54,Sedov_Anton,Iphone_XR,1000
11,2021_11_12_09:52,Sedov_Anton,Case_Iphone_XR,10
12,2021_11_13_12:12,Kilov_Andrey,Samsung_S10,1000
13,2021_11_13_14:25,Jukov_Andrey,Iphone_7,300
```


## Usage util

`python main.py [-add] [-analysis]`


## Optional 

### Show help message

`python main.py [--help] [-h]`

``` 
Utility for storing, adding and analyzing orders

optional arguments:
  -h, --help            show this help message and exit
  -analysis             Data output for every hour for the last 3 days
  -add                  Add order to base
   

Example: python main.py -analysis go
         python main.py -add 2021_11_12_11:45,Ivanov_Ivan,Iphone_XR,500
```
### Usage examples

##### Data output for every hour for the last 3 days
`python main.py -analysis go`
```
Date,Number of orders,Total amount of orders,Number of unique clients
2021 11 9 09~10,1,200$,1
2021 11 10 10~11,1,500$,1
2021 11 10 11~12,1,1000$,1
2021 11 10 12~13,1,800$,1
2021 11 11 15~16,3,2120$,2
2021 11 11 16~17,2,1500$,2
```
##### Add order to base
`python main.py -add 2021_11_13_19:37,Matviyenko_Vilena,Iphone_XR,800`

```
...
10,2021_11_11_16:54,Sedov_Anton,Iphone_XR,1000
11,2021_11_12_09:52,Sedov_Anton,Case_Iphone_XR,10
12,2021_11_13_12:12,Kilov_Andrey,Samsung_S10,1000
13,2021_11_13_14:25,Jukov_Andrey,Iphone_7,300
14,2021_11_13_19:37,Matviyenko_Vilena,Iphone_XR,800
```
import csv
import datetime

def add_order(order):

	"""
	Function for adding an order to the database
	"""
	with open('base.csv','r') as f:
		opened_file = f.readlines()
		last_record = opened_file[-1].split(',')[0]
		try:
			last_record = int(last_record)
		except:
			last_record = 0
	
	new_record = [(last_record +1)] + order.split(',')
	with open('base.csv', 'a',newline = '') as file:
		writer = csv.writer(file)
		writer.writerows([new_record])


def last_3_days_data():

	"""
	Function to get data for the last 3 days
	"""

	today = (str(datetime.datetime.now()).split(' ')[0]).split('-')[1:3]
	month_now = today[0]
	day_now = today[1]
	data = []
	with open('base.csv',newline = '') as file:
		reader = csv.DictReader(file,delimiter = ',')
		for row in reader:
			_,month,day,time = row['date'].split('_')
			if month_now == month:
				if 0 <= int(day_now) - int(day) < 3:
					data += [[row['date'],row['buyer_name'],row['price']]]

	
	return data


def output_analysis():

	"""
	Function for outputting data for every hour
	"""
	data = last_3_days_data()
	hours_data = {}
	for date,name,price in data:
		time = (date.split(':')[0])+ '~' + f"{int((date.split(':')[0])[-2:]) + 1}"
		if time in hours_data:
			hours_data[time][0] += 1
			hours_data[time][1] += int(price)
			if name not in hours_data[time][2]:
				hours_data[time][2].append(name)
				hours_data[time][3] +=1
		else:
			hours_data[time] = [0,0,[],0]
			hours_data[time][0] = 1
			hours_data[time][1] = int(price)
			hours_data[time][2].append(name)
			hours_data[time][3] = 1

	print('Date,Number of orders,Total amount of orders,Number of unique clients')
	for date in hours_data:
		normal_date = date.split('_')
		amount = hours_data[date][0]
		price = hours_data[date][1]
		amount_clients = hours_data[date][3]
		print(f"{' '.join(normal_date)},{amount},{price}$,{amount_clients}")

